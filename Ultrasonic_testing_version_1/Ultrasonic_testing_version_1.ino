#include <LiquidCrystal.h>
String myAPIkey = "KGPR4HEUFSTYLF9T"; //Status channel id :661233
#include <SoftwareSerial.h>
SoftwareSerial ESP8266(0,1);//Rx,Tx 
LiquidCrystal lcd (12,11,5,4,3,2);
long writingTimer = 10;//Upload data on thingspeak
long startTime = 0;
long waitTime = 0;
int trig = 10;
int led =13;
int echo = 9;
long duration;
int distance;
int person=0;
int d1=0;
int d2=0;
boolean relay1_st = false; 
boolean relay2_st = false; 
unsigned char check_connection=0;
unsigned char times_check=0;
boolean error;
//int relay =11;
void setup()
{ 
  lcd.begin(16,2);
  //pinMode(11 , OUTPUT);
  pinMode(13 , OUTPUT);
  pinMode( 10 , OUTPUT );
  pinMode( 9 , INPUT);
  Serial.begin(9600);
  ESP8266.begin(9600);//Communication rate of wifi module
  startTime = millis();//Starting program clock
  ESP8266.println("AT+RST"); //Reseting the ESP8266 whenever system starts
  delay(5000);
  Serial.println("connecting to wifi");
   while(check_connection==0)
  {
    Serial.print(".");
    ESP8266.print("AT+CWJAP=\"ARNAV\",\"TUNTUN12345\"\r\n");
    ESP8266.setTimeout(5000);
     if(ESP8266.find("WIFI CONNECTED\r\n")==1)
     {
       Serial.println("WIFI CONNECTED");
       break;
     }
       times_check++;
     if(times_check>3) 
     {
      times_check=0;
       Serial.println("Trying to Reconnect..");
      }
   }
}


void loop()
{

  waitTime = millis()-startTime;   
  if (waitTime > (writingTimer*1000)) 
  {
    readSensors();
    writeThingSpeak();
    startTime = millis();   
  }

}
void readSensors(void)
{
  digitalWrite( trig ,LOW);
  delayMicroseconds(2);
  digitalWrite( trig ,HIGH);
  delayMicroseconds(10);
  digitalWrite( trig ,LOW);
  duration = pulseIn( echo, HIGH);
  Serial.print(duration);
  Serial.print("\t");
  distance = (duration * 0.034)/2;
  Serial.print(distance);
  Serial.print("\t");
  delay(500);
  lcd.setCursor(0,0); // Sets the location at which subsequent text written to the LCD will be displayed
  lcd.print("Distance: "); // Prints string "Distance" on the LCD
  lcd.print(distance); // Prints the distance value from the sensor
  lcd.print("  cm");
  delay(50);
   if( distance < 50)
 {
  Serial.println(" relay on");
  //digitalWrite( relay , LOW);
  digitalWrite( led , HIGH);
  lcd.setCursor(2,1);
  lcd.print("obj detected");
   d1=distance;
   delay(30000);
   d2 = distance;
   if(d1==d2)
   {
    person=person+1;
   }
 }
 else
 {
  Serial.println(" relay off");
  digitalWrite( led ,LOW);
  //digitalWrite( relay , HIGH);
  lcd.setCursor(2,1);
  lcd.print("not detected");
 }
  
void writeThingSpeak(void)
{
  startThingSpeakCmd();
  // preparacao da string GET
  String getStr = "GET /update?api_key=";
  getStr += myAPIkey;
  getStr +="&field1=";
  getStr += String(distance);
  getStr +="&field2=";
  getStr += String(person);
  getStr += "\r\n\r\n";
  GetThingspeakcmd(getStr); 
}

void startThingSpeakCmd(void)
{
  ESP8266.flush();
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // api.thingspeak.com IP address
  cmd += "\",80";
  ESP8266.println(cmd);
  Serial.print("Start Commands: ");
  Serial.println(cmd);

  if(ESP8266.find("Error"))
  {
    Serial.println("AT+CIPSTART error");
    return;
  }
}

String GetThingspeakcmd(String getStr)
{
  String cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ESP8266.println(cmd);
  Serial.println(cmd);

  if(ESP8266.find(">"))
  {
    ESP8266.print(getStr);
    Serial.println(getStr);
    delay(500);
    String messageBody = "";
    while (ESP8266.available()) 
    {
      String line = ESP8266.readStringUntil('\n');
      if (line.length() == 1) 
      { 
        messageBody = ESP8266.readStringUntil('\n');
      }
    }
    Serial.print("MessageBody received: ");
    Serial.println(messageBody);
    return messageBody;
  }
  else
  {
    ESP8266.println("AT+CIPCLOSE");     
    Serial.println("AT+CIPCLOSE"); 
  } 
}
