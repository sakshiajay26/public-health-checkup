int trig= D0;
int ech=D1;
int led=13;
long duration;
int distance;
void setup() 
{
  pinMode(D0,OUTPUT);
  pinMode(D1, INPUT);
  pinMode(13,OUTPUT);
  Serial.begin(9600);
}

void loop() 
{
  digitalWrite( trig ,LOW);
  delayMicroseconds(2);
  digitalWrite( trig ,HIGH);
  delayMicroseconds(10);
  digitalWrite( trig ,LOW);
  duration = pulseIn( ech, HIGH);
  Serial.print(duration);
  Serial.print("\t");
  distance = (duration * 0.034)/2;
  Serial.print(distance);
  Serial.print("\t");
  delay(500);
  if(distance<50)
  {
  Serial.println(" relay on");
  digitalWrite( 13 , HIGH);
  }
 else
 {
  Serial.println(" relay off");
  digitalWrite( 13 , LOW);
 }
}
