
#include "ThingSpeak.h"
#include "secrets.h"




//SoftwareSerial gsm(3, 1); //rx,tx

char ssid[] = SECRET_SSID;   // your network SSID (name)
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

int sensor = A0;
int beeper = D6;
int trig = D0;
int ech  = D1;
int led  = D2;
long duration;
int distance;
long duration1;
int distance1;
long duration2;
int distance2;
int relay1 = D3;
int relay2 = D4;
int relay3 = D5;
int relay4 = D7;
int gasLevel = 0;
String quality = "";

int personNo = 0;

void setup()
{
  Serial.begin(115200);  // Initialize serial snd fix buad rate to this value in serial monitor
  pinMode(D0, OUTPUT);
  pinMode(D1, INPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(A0, INPUT);
  WiFi.mode(WIFI_STA);
  ThingSpeak.begin(client);  // Initialize ThingSpeak
}

void loop()
{
  digitalWrite( trig , LOW);
  delayMicroseconds(2);
  digitalWrite( trig , HIGH);
  delayMicroseconds(10);
  digitalWrite( trig , LOW);
  duration = pulseIn( ech, HIGH);
  distance = (duration * 0.034) / 2;
  Serial.print("Distance:");
  Serial.println(distance);
  if (distance < 15)
  {
    digitalWrite( trig , LOW);
    delayMicroseconds(2);
    digitalWrite( trig , HIGH);
    delayMicroseconds(10);
    digitalWrite( trig , LOW);
    duration1 = pulseIn( ech, HIGH);
    distance1 = (duration1 * 0.034) / 2;
    Serial.print("Distance1:");
    Serial.print(distance1);
    Serial.print("\t");
    delay(1000);
    digitalWrite( trig , LOW);
    delayMicroseconds(2);
    digitalWrite( trig , HIGH);
    delayMicroseconds(10);
    digitalWrite( trig , LOW);
    duration2 = pulseIn( ech, HIGH);
    distance2 = (duration2 * 0.034) / 2;
    Serial.print("Distance2:");
    Serial.println(distance2);
    if ((distance2 <= distance1 + 3 ) && (distance2 >= distance1 - 3))
    {
      Serial.println("person Using Toilet");
      personNo++;
      digitalWrite( D3 , LOW);
      digitalWrite( D4 , LOW);
      digitalWrite( D5 , LOW);
    }
  }
  else
  {
    digitalWrite( D3 , LOW);
    delay(3000);
    digitalWrite( D3 , HIGH);
    digitalWrite( D4 , HIGH);
    digitalWrite( D5 , HIGH);
    digitalWrite( D7 , HIGH);
    Serial.println(" Green ON");

    gasLevel = analogRead(sensor);
    Serial.println(gasLevel);
    if (gasLevel < 175) {
      quality = "GOOD!           ";
      digitalWrite(D6, LOW);
      Serial.println(quality);
      delay(100);
    }
    else if (gasLevel > 175 && gasLevel < 225) {
      quality = "Something stinks ";
      digitalWrite(D6, HIGH);
      delay(5000);
      digitalWrite(D6, LOW);
      Serial.println(quality);
      delay(100);
    }
    else if (gasLevel > 225 && gasLevel < 300)
    { 
     
      quality = "Moderately polluted";
      digitalWrite(D6, HIGH);
      delay(5000);
      digitalWrite(D6, LOW);
      Serial.println(quality);
      delay(1000);
    }
    else if (gasLevel > 300)
    {


      quality = "Toilet Needs Cleaning ";
      digitalWrite( D7 , HIGH);
      Serial.println( "Red ON");
      digitalWrite(D6, HIGH);
      delay(5000);
      digitalWrite(D6, LOW);
      Serial.println(quality);
      delay(1000);
    }
    else
    {
      digitalWrite(D6, LOW);
    }
  }

  // Connect or reconnect to WiFi
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while (WiFi.status() != WL_CONNECTED) {
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);
    }
    Serial.println("\nConnected.");
  }

  // set the fields with the values
  ThingSpeak.setField(1, distance);
  ThingSpeak.setField(2, personNo);
  ThingSpeak.setField(3, gasLevel);
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if (x == 200) {
    Serial.println(" Channel update successful.");
  }
  else {
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }
  delay(5000);

}
