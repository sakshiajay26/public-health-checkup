/*
   Arduino Air Quality Monitor

*/



//#include <LiquidCrystal.h>                //LCD Library
#include <SoftwareSerial.h>
SoftwareSerial gsm(6, 7); //rx,tx
#define beeper    8                       //beeper on Digital 8
#define sensor    A0                      //sensor on Analog 0
int trig = 10;
int ech = 9;
int led = 13;
long duration;
int distance;
int gas1;
int gas2;
int relay1 = 2;
int relay2 = 3;
int relay3 = 4;
//LiquidCrystal lcd(12, 11, 5, 4, 3, 2);    //Create instance of LCD

int gasLevel = 0;                         //int variable for gas level
String quality = "";

void setup()
{

  delay(10000);
  Serial.begin(9600);                     //start serial comms
  gsm.begin(9600);
  pinMode(beeper, OUTPUT);                //set beeper for output
  pinMode(sensor, INPUT);                 //set sensor for input
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, INPUT);
  pinMode(13, OUTPUT);

  /*lcd.begin(16, 2);                       //initial LCD setup
    lcd.setCursor (0, 0);                   //splash screen and warmup
    lcd.print("                ");
    lcd.setCursor (0, 1);
    lcd.print("                ");
    lcd.setCursor (0, 0);
    lcd.print("   Air Sensor   ");
    lcd.setCursor (0, 1);
    lcd.print("   Warming Up   ");
    delay(2000);                            //set for at least 2 minutes

    lcd.setCursor (0, 0);                   //clear screen
    lcd.print("                ");
    lcd.setCursor (0, 1);
    lcd.print("                ");
  */
}

void loop()
{
  digitalWrite( trig , LOW);
  delayMicroseconds(2);
  digitalWrite( trig , HIGH);
  delayMicroseconds(10);
  digitalWrite( trig , LOW);
  duration = pulseIn( ech, HIGH);
  Serial.print("Duration:");
  Serial.print(duration);
  Serial.print("\t");
  distance = (duration * 0.034) / 2;
  Serial.print("Distance:");
  Serial.print(distance);
  Serial.print("\t");
  delay(1000);
  if (distance < 50)
  { digitalWrite(13, HIGH);
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
  }
  else
  {
    digitalWrite(13, LOW);
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
    gasLevel = analogRead(sensor);
    Serial.println(gasLevel);
    if (gasLevel < 175) {
      quality = "GOOD!           ";
      digitalWrite(8, LOW);
      Serial.println(quality);
      delay(1000);
    }
    else if (gasLevel > 175 && gasLevel < 225) {
      quality = "Did you fart?   ";
      digitalWrite(8, HIGH);
      Serial.println(quality);
      delay(1000);
    }
    else if (gasLevel > 225 && gasLevel < 300)
    {
      /*gas1 = gasLevel;
        delay(10000);
        gas2 = gasLevel;
        if ((gas2 <= gas1 + 5) && (gas2 >= gas1 - 5))
        {*/
      SendTextMessage();
      quality = "Something dead?";
      digitalWrite(8, HIGH);
      Serial.println(quality);
      delay(1000);
    }
    else if (gasLevel > 300)
    {
      /*gas1 = gasLevel;
        delay(10000);
        gas2 = gasLevel;
        if ((gas2 <= gas1 + 50) && (gas2 >= gas1 - 50))
        {*/
      SendTextMessage();
      quality = "Pretty fk'n bad ";
      digitalWrite(8, HIGH);
      Serial.println(quality);
      delay(1000);
    }
    else
    {
      digitalWrite(8, LOW);
    }
    /*lcd.setCursor (0, 0);
    lcd.print("Air Quality is:");
    lcd.setCursor(0, 1);
    lcd.print(quality);*/
  }
}

void SendTextMessage()
{
  gsm.println("AT+CMGF=1");    //To send SMS in Text Mode
  delay(1000);
  gsm.println("AT+CMGS=\"+918454880536\"\r");  // change to the phone number you using
  delay(1000);
  gsm.println("Toilet needs a cleaning");//the content of the message
  delay(200);
  gsm.println((char)26);//the stopping character
  delay(1000);

}


/*
  The circuit:
   LCD RS pin to digital pin 12
   LCD Enable pin to digital pin 11
   LCD D4 pin to digital pin 5
   LCD D5 pin to digital pin 4
   LCD D6 pin to digital pin 3
   LCD D7 pin to digital pin 2
   LCD R/W pin to ground
   LCD VSS pin to ground
   LCD VCC pin to 5V
   5K pot:
   ends to +5V and ground
   wiper to LCD VO pin (pin 3)

*/
