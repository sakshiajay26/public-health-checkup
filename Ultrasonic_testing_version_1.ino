int trig = 10;
int led =13;
int echo = 9;
long duration;
int distance;
int relay =2;
void setup()
{
  pinMode(2 , OUTPUT);
  pinMode(13 , OUTPUT);
  pinMode( 10 , OUTPUT );
  pinMode( 9 , INPUT);
  Serial.begin(9600);
  
}

void loop()
{
  digitalWrite( trig ,LOW);
  delayMicroseconds(2);
  digitalWrite( trig ,HIGH);
  delayMicroseconds(10);
  digitalWrite( trig ,LOW);
  duration = pulseIn( echo, HIGH);
  Serial.print(duration);
  Serial.print("\t");
 distance = (duration * 0.034)/2;
 Serial.print(distance);
 Serial.print("\t");
 delay(500);
 if( distance < 50)
 {
  Serial.println(" relay on");
 digitalWrite( relay , LOW);
 digitalWrite( led , HIGH);
 }
 else
 {
  Serial.println(" relay off");
 digitalWrite( led ,LOW);
 digitalWrite( relay , HIGH);
 }
}
